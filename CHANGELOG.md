# Changelog

### [3.0.1](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/compare/v3.0.0...v3.0.1) (2022-06-29)

### Correções/Alterações

* atualizar id do tag manager environment.prod.ts ([8e6e281](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/8e6e281f724eeb46b592bfd8a3f34615b6d80654))
* atualizar id tag manager environment.ts ([36b7f50](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/36b7f5059768ab0afc4d8109ba667a2aec14a958))
* coloca pipeline teste ([08ab16a](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/08ab16a12ee34501ca2bbaa0c7c405863660e7d4))
* coloca pipeline teste ([12ee044](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/12ee0448108395e3e4cd5af28034f7205710583a))
* coloca a versao do google tag para 1.4.2 ([8f9fa9d](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/8f9fa9d2aaaa2111906c92a6ce055f36593cb66a))
* gitcorporativo para gitlab ([a9c62af](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/a9c62af83acf9ef8a94655b8e65e8f5a7250a0cf))
* retira o texto designer system do governo federal ([cd285b7](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-site/commit/cd285b70bdf0df4d65c649d48110bb47045e9141))
* **lint scss:** remove display box ([c10a1ca](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-core/commit/c10a1ca4b07b724790eb3bff392dea8ec9374564))
* **list:** corrige checkbox dentro de lista ([17f6052](https://gitlab.com/govbr-ds/govbr-ds-dev/govbr-ds-dev-core/commit/17f60521306bfb42f39836a37f262437113eff8b))
* alterado os navegadores suportados ([25ecdde6](https://gitlab.com/govbr-ds/ds/dev/govbr-ds-dev-core/-/commit/25ecdde639d56252cc4030feb97b14e64b1e614a))
