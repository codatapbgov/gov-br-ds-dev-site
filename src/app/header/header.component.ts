import { Component } from '@angular/core'
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { AfterViewInit, ElementRef, OnInit } from '@angular/core'
import algoliasearch from 'algoliasearch/lite'

import { Tooltip } from '@pbgov-ds/core/dist/core'
import { environment } from 'src/environments/environment'
import { FilesService } from '../services/files.service'
import { StorageService } from '../services/storage.service'

const searchClient = algoliasearch('7T053RA1DW', '0b72c67626dc3e56c0b012ae9232e8f6')

@Component({
	selector: 'br-header',
	styleUrls: ['./header.component.scss'],
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, AfterViewInit {
	header: HTMLCollection
	version: string
	showMenu: boolean
	showMenuActions: boolean
	showMenuLinks: boolean
	toogleSearch: boolean

	config = {
		indexName: 'prod',
		searchClient,
		numberLocale: 'pt-BR',
	}

	constructor(private fileService: FilesService, private storageService: StorageService, private elem: ElementRef) {}

	ngOnInit() {
		this.header = document.getElementsByClassName('br-header')

		this.storageService.currentState.subscribe((showMenu) => {
			this.showMenu = showMenu
		})

		this.fileService.fetchData(`${environment.docUrl}/package.json`).subscribe((data: any) => {
			this.version = data.version
		})

		this.setSticky()
		this.showMenuActions = false
		this.showMenuLinks = false
	}

	ngAfterViewInit() {
		this.setTooltip()
	}

	setSticky() {
		window.onscroll = () => {
			if (window.pageYOffset > this.header[0]['offsetHeight']) {
				this.header[0].classList.add('sticky', 'compact')
				this.header[0].parentElement.setAttribute('compact', '')
			} else {
				this.header[0].classList.remove('sticky', 'compact')
				this.header[0].parentElement.removeAttribute('compact')
			}
		}
	}

	toggleMenu() {
		this.storageService.toogleMenu()
	}

	toggleMenuActions() {
		this.showMenuActions = !this.showMenuActions
	}

	toggleMenuLinks() {
		this.showMenuLinks = !this.showMenuLinks
	}

	showSearch() {
		let search = this.elem.nativeElement.querySelector(`.header-search`)
		search.classList.add('active')
	}

	closeMenuActions() {
		setTimeout(() => {
			this.showMenuActions = false
		}, 250)
	}

	closeMenuLinks() {
		setTimeout(() => {
			this.showMenuLinks = false
		}, 250)
	}

	setTooltip() {
		const TooltipExampleList = []

		this.elem.nativeElement.querySelectorAll('[data-tooltip-target]').forEach((trigger) => {
			const targets = document.querySelectorAll(trigger.getAttribute('data-tooltip-target'))
			targets.forEach((target) => {
				const place = target.getAttribute('place') !== null ? target.getAttribute('place') : 'top'
				const config = {
					activator: trigger,
					component: target,
					place: place,
					placement: 'top',
					type: 'warning',
				}
				const tooltip = new Tooltip(config)
			})
		})
	}
}
