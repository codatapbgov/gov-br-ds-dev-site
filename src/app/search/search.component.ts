import { Component, ElementRef, forwardRef, HostListener, Inject, OnInit, Optional, ViewChild } from '@angular/core'
import { NgAisIndex, NgAisInstantSearch, TypedBaseWidget } from 'angular-instantsearch'
import connectSearchBox, {
	SearchBoxConnectorParams,
	SearchBoxWidgetDescription
} from 'instantsearch.js/es/connectors/search-box/connectSearchBox'
import { debounceTime, distinctUntilChanged, filter, fromEvent } from 'rxjs'

@Component({
	selector: 'br-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss'],
})
export class SearchComponent
	extends TypedBaseWidget<SearchBoxWidgetDescription, SearchBoxConnectorParams>
	implements OnInit
{
	public state: SearchBoxWidgetDescription['renderState']
	@ViewChild('input', { static: false }) input: ElementRef
	public searchQuery: string = ''

	constructor(
		@Inject(forwardRef(() => NgAisIndex))
		@Optional()
		public parentIndex: NgAisIndex,
		@Inject(forwardRef(() => NgAisInstantSearch))
		public instantSearchInstance: NgAisInstantSearch,
		private elem: ElementRef
	) {
		super('SearchBox')
	}

	ngOnInit() {
		this.createWidget(connectSearchBox, {})
		super.ngOnInit()
	}

	@HostListener('document:click', ['$event.target'])
	public onClick(target) {
		const clickedInside = this.elem.nativeElement.contains(target)
		let aisInfineHits = this.elem.nativeElement.querySelector('ais-infinite-hits')
		if (!clickedInside && aisInfineHits && !aisInfineHits.classList.contains('d-none')) {
			aisInfineHits.classList.add('d-none')
		} else if (clickedInside && aisInfineHits && aisInfineHits.classList.contains('d-none')) {
			aisInfineHits.classList.remove('d-none')
		}
	}

	/**
	 * Limita buscas repetidas e por tempo após digitação
	 */
	searchOnType(event, value) {
		let nValue = value.trim()
		let loading = this.elem.nativeElement.querySelector(`.br-item.load`)
		let aisInfineHits = this.elem.nativeElement.querySelector('ais-infinite-hits')
		if (nValue.length > 3 && event.keyCode != 38 && event.keyCode != 40) {
			loading.classList.remove('d-none')
			if (aisInfineHits) aisInfineHits.classList.add('d-none')
		}
		fromEvent(this.input.nativeElement, 'keyup')
			.pipe(
				// Só pesquisa quando tiver pelo menos 4 caracteres
				filter(() => nValue.length > 2 || (event.keyCode == 13 && nValue.length > 0)),
				// Com 1 segundo entre as buscas
				debounceTime(1000),
				// E se o valor é diferente o anterior
				distinctUntilChanged()
			)
			.subscribe(() => {
				this.searchQuery = this.input.nativeElement.value
				this.state.refine(this.input.nativeElement.value)
				loading.classList.add('d-none')
				if (aisInfineHits) aisInfineHits.classList.remove('d-none')
			})
	}

	clearSearch() {
		// Método para limpar o query dentro do instantasearch.js (algolia)
		this.state.clear()
		// Limpar o input
		this.input.nativeElement.value = ''
		// Limpar a variável usada para mostrar os icones
		this.searchQuery = ''
	}

	closeSearch() {
		let search = this.elem.nativeElement.querySelector(`.header-search`)
		if (search.classList.contains('active')) {
			search.classList.remove('active')
		}
	}
}
