import { CommonModule } from '@angular/common'
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http'
import { NgModule, SecurityContext } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { Router } from '@angular/router'
import { NgAisModule, NgAisStatsModule } from 'angular-instantsearch'
import { CookieService } from 'ngx-cookie-service'
//import { NgxHotjarModule, NgxHotjarRouterModule } from 'ngx-hotjar'
import { MarkdownModule, MarkedOptions, MarkedRenderer } from 'ngx-markdown'
//import { environment } from './../environments/environment.prod'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { AvaliacaoComponent } from './avaliacao/avaliacao.component'
import { CodeComponent } from './code/code.component'
import { CollapseDirective } from './collapse.directive'
import { ContentComponent } from './content/content.component'
import { DownloadComponent } from './download/download.component'
import { FooterComponent } from './footer/footer.component'
import { HeaderComponent } from './header/header.component'
import { Interceptor } from './interceptors/interceptor.service'
import { MenuComponent } from './menu/menu.component'
import { NavKeyboardDirective } from './navkeyboard.directive'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { SafeHtmlPipe } from './pipes/safe-html.pipe'
import { ScrollTopComponent } from './scroll-top/scroll-top.component'
import { SearchComponent } from './search/search.component'
import { NavigationService } from './services/navigation.service'
import { TabsComponent } from './tabs/tabs.component'

export function markedOptionsFactory(NavigationService: NavigationService): MarkedOptions {
	const renderer = new MarkedRenderer()

	renderer.link = (href, title, text) => {
		return MarkedRenderer.prototype.link.call(renderer, NavigationService.normalizeExternalUrl(href), title, text)
	}

	return { renderer }
}

@NgModule({
	declarations: [
		AppComponent,
		FooterComponent,
		HeaderComponent,
		MenuComponent,
		PageNotFoundComponent,
		DownloadComponent,
		ContentComponent,
		CodeComponent,
		ScrollTopComponent,
		TabsComponent,
		CollapseDirective,
		NavKeyboardDirective,
		SafeHtmlPipe,
		SearchComponent,
		AvaliacaoComponent,
	],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		//NgxHotjarModule.forRoot(environment.HJID),
		//NgxHotjarRouterModule,
		NgAisStatsModule,
		NgAisModule.forRoot(),
		ReactiveFormsModule,
		CommonModule,
		AppRoutingModule,
		CommonModule,
		HttpClientModule,
		FormsModule,
		MarkdownModule.forRoot({
			loader: HttpClient,
			markedOptions: {
				provide: MarkedOptions,
				useFactory: markedOptionsFactory,
				deps: [NavigationService],
				useValue: {
					breaks: true,
					smartLists: true,
					smartypants: true,
					tables: true,
				},
			},
			sanitize: SecurityContext.NONE,
		}),
	],
	exports: [],
	providers: [
		//{ provide: 'googleTagManagerId', useValue: environment.YOUR_GTM_ID },
		Interceptor,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: Interceptor,
			multi: true,
		},
		[CookieService],
	],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor(public router: Router) {}
}
