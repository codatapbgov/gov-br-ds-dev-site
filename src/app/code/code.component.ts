import { Component, ElementRef, OnInit, QueryList, ViewChildren } from '@angular/core'
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { environment } from 'src/environments/environment'
import { ISelectedFile } from '../interfaces/selectedFile'
import { ITab } from '../interfaces/tab'
import { FilesService } from '../services/files.service'
import { StorageService } from '../services/storage.service'
import { IPreviewTab } from './../interfaces/previewTab'
import { IPreviewTabs } from './../interfaces/previewTabs'
import { UtilService } from './../services/util.service'

@Component({
	selector: 'br-code',
	styleUrls: ['./code.component.scss'],
	templateUrl: './code.component.html',
})
export class CodeComponent implements OnInit {
	@ViewChildren('iframe') iframes: QueryList<ElementRef>
	selectedFile: ISelectedFile
	// Códigos usados por todos os exemplos
	codeTabs: ITab[]
	// Exemplos
	previewTabs: IPreviewTabs[]

	previewUrl: string

	constructor(
		private storageService: StorageService,
		private fileService: FilesService,
		private sanitizer: DomSanitizer,
		private utilService: UtilService
	) {}

	ngOnInit(): void {
		this.storageService.currentSelectedFile.subscribe((selectedFile: ISelectedFile) => {
			this.createTabs()
			if (['components', 'templates', 'pages', 'util'].includes(selectedFile?.type)) {
				// Limpa as abas sempre que o arquivo selecionado for trocado

				if (selectedFile) this.selectedFile = Object.assign(selectedFile)

				if (this.selectedFile?.ext) {
					this.setContent()
				}
			}
		})
	}

	/**
	 * Inicializa as abas e codePen
	 */
	createTabs() {
		this.codeTabs = [
			{
				ext: 'html',
				id: 1,
				label: 'HTML',
				lang: 'html',
			},
			{
				ext: 'css',
				id: 2,
				label: 'CSS',
				lang: 'css',
			},
			{
				ext: 'js',
				id: 3,
				label: 'Javascript',
				lang: 'javascript',
			},
		]

		this.previewTabs = []
	}

	setContent() {
		// Remove a extensão 'md' pois é apenas para documentação
		const dontFetchThis = ['md']

		// Só busca o HTML grande se não existir exemplos separados
		if (this.selectedFile.examples) {
			dontFetchThis.push('html')

			// Cria uma nova instância do array examples
			const examples = [...this.selectedFile.examples]

			examples.forEach((example) => {
				const path = this.createExampleUrl(example.path)
				this.fetchExamplesData(example, path)
			})
		}

		const extensionsToFetch = this.selectedFile.ext.filter((ext: string) => {
			return !dontFetchThis.includes(ext)
		})

		// Para cada extensão busca o conteúdo e coloca no this.tabs
		extensionsToFetch.forEach((ext: string) => {
			const url = this.createUrls(ext)
			this.fetchData(url, ext)
		})

		this.removeBlankCodeTabs(extensionsToFetch)
	}

	fetchData(url: string, ext: string) {
		this.fileService.fetchData(url, 'text').subscribe((data) => {
			const code = this.fileService.fixUrls(String(data), url)
			this.setCodeTabs(code, ext)
		})
	}

	fetchExamplesData(example: IPreviewTab, path: string) {
		this.fileService.fetchData(String(path), 'text').subscribe((data) => {
			const content = this.appendElementsToPreview(String(data))

			const iframeTab = {
				id: this.generateRandomID(),
				label: 'Preview',
				path: path,
				url: this.sanitizer.bypassSecurityTrustResourceUrl(path),
				content: this.sanitizer.bypassSecurityTrustHtml(content),
				unsafeContent: String(data),
				isPreview: true,
			}

			const htmlTab = {
				id: this.generateRandomID(),
				label: 'HTML',
				path: path,
				url: this.sanitizer.bypassSecurityTrustResourceUrl(path),
				content: this.sanitizer.bypassSecurityTrustHtml(content),
				unsafeContent: String(data),
				isPreview: false,
				lang: 'html',
			}

			this.previewTabs.push({
				label: example.label,
				tabs: [iframeTab, htmlTab],
				height: example.height,
			})
		})
	}

	setCodeTabs = (code: string, ext: string) => {
		this.codeTabs = this.codeTabs.map((item) => {
			if (item.ext === ext) {
				item.content = code
			}

			return item
		})
	}

	createPreviewTab(url: string) {
		if (!this.selectedFile.examples) {
			const previewTab = {
				id: 0,
				label: 'Preview',
				isPreview: true,
				url: this.sanitizer.bypassSecurityTrustResourceUrl(url),
			}
			this.codeTabs.unshift(previewTab)
		}
	}

	/**
	 * Cria as URLs paga o CSS, JS e HTML
	 */
	createUrls(ext: string) {
		let url = `${environment.baseUrl}/`
		let folder = 'ds/dist'
		const type = this.selectedFile.type
		let component = this.selectedFile.id
		let filename = this.selectedFile.id

		/**
		 * scss: src/component/_component
		 * js: src/component/component
		 * css: dist/component/component
		 * html: dist/component|pages/component|examples
		 */
		if (ext === 'html') {
			// Componentes tem uma nomenclatura diferente dos templates
			if (['components', 'pages', 'util'].includes(this.selectedFile.type)) filename = 'examples'
		}
		if (ext === 'scss') {
			folder = 'src'
			component = `_${this.selectedFile.id}`
		}

		// if (ext === 'js') {
		// 	folder = 'ds'
		// }

		url += `${folder}/${type}/${component}/${filename}.${ext}`

		if (ext === 'html' && !this.selectedFile.examples) {
			this.previewUrl = url
			this.createPreviewTab(url)
		}

		return url
	}

	/**
	 * Cria as URLs dos exemplos
	 */
	createExampleUrl(path: SafeResourceUrl) {
		const type = this.selectedFile.type
		const item = this.selectedFile.id
		const url = `${environment.baseUrl}/ds/dist/${type}/${item}/${path}.html`
		return url
	}

	/**
	 * Caso a extensão na aba não exista na lista de extensões para baixar, ela será removida da lista
	 * Exceção é a aba Preview (id=0)
	 */
	removeBlankCodeTabs(extensionsToFetch: string[]) {
		this.codeTabs = this.codeTabs.filter((tab) => {
			const fetchTab = extensionsToFetch.some((ext) => ext === tab.ext || tab.id === 0)
			return fetchTab
		})
	}

	/**
	 * Esse é o responsável por buscar o código que falta e montar na variável this.codePenData
	 */
	setCodePenData(unsafeContent: string) {
		const title = this.setCodePenTitle('GOV.BR-DS')
		const tags = this.setCodePenTags(['GOV.BR-DS'])

		/**
		 * Essa variável deve seguir o formado definido pela API do codePen
		 * https://blog.codepen.io/documentation/prefill/
		 */
		const codePenData = {
			title: title,
			description: 'Exemplo de código do Design System GOV.BR (https://gov.br/ds)',
			html: unsafeContent,
			tags: tags,
			css_pre_processor: 'css',
			css: null,
			js: null, // Não mostrar poque estava dando conflito com o nome das classes no codePen
			editors: '1',
			css_external: [
				'https://docs-ds.estaleiro.serpro.gov.br/docs/ds/dist/core.min.css',
				'https://cdngovbr-ds.estaleiro.serpro.gov.br/design-system/fonts/rawline/css/rawline.css',
				'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css',
			],
			js_external: 'https://docs-ds.estaleiro.serpro.gov.br/docs/ds/dist/core-init.js',
		}

		this.codeTabs.forEach((tab) => {
			// Setava js e css, mas foi retirado
			// codePenData[tab.ext] = tab.content

			if (tab.ext === 'css') codePenData.css = tab.content
		})

		this.setEditorConfig(codePenData)

		return codePenData
	}

	setCodePenTitle(title: string) {
		if (this.selectedFile.type) {
			title += ` - ${this.utilService.toTitleCase(this.selectedFile.type)}`
		}

		if (this.selectedFile.label) {
			title += ` - ${this.selectedFile.label}`
		}
		return title
	}

	setCodePenTags(tags: Array<string>) {
		if (this.selectedFile.type) {
			tags.push(this.selectedFile.type)
		}

		if (this.selectedFile.label) {
			tags.push(this.selectedFile.label)
		}
		return tags
	}

	// editors 111 => HTML, CSS, JS
	setEditorConfig(codePenData: any) {
		if (codePenData.css) codePenData.editors += 1
		if (!codePenData.css) codePenData.editors += 0
		if (codePenData.js) codePenData.editors += 1
		if (!codePenData.js) codePenData.editors += 0
	}

	/**
	 * Ao clicar seta o valor do input hidden do form pra chamar a API do codePen
	 */
	sendToCodePen(unsafeContent: any, id: string) {
		const codePenData = this.setCodePenData(unsafeContent)
		const codePen = <HTMLInputElement>document.getElementById(id)
		codePen.value = JSON.stringify(codePenData)
	}

	appendElementsToPreview(data: string): string {
		const elementsToAppend = this.createElementsToAppend()
		data = data.concat(...elementsToAppend)
		return data
	}

	createElementsToAppend() {
		const css = document.createElement('link')
		css.rel = 'stylesheet'
		css.href = 'https://docs-ds.estaleiro.serpro.gov.br/docs/ds/dist/core.css'

		const rawline = document.createElement('link')
		rawline.rel = 'stylesheet'
		rawline.href = 'assets/fonts/rawline/css/rawline.css'

		const fontawesome = document.createElement('link')
		fontawesome.rel = 'stylesheet'
		fontawesome.href = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css'

		const js = document.createElement('script')
		js.src = 'https://docs-ds.estaleiro.serpro.gov.br/docs/ds/dist/core-init.js'

		const fixPreview = document.createElement('style')
		fixPreview.innerHTML = 'body{padding:1rem;}'

		return [css.outerHTML, rawline.outerHTML, fontawesome.outerHTML, js.outerHTML, fixPreview.outerHTML]
	}

	onIFrameLoad($event: any) {
		this.resizeIFrame($event.target)
		$event.target.contentDocument.body.addEventListener('mouseup', (element) => this.onIFrameClick(element))
	}

	onIFrameClick(element: any) {
		// Como o iframe é um ambiente isolado, o jeito mais fácil é esperar o evento ocorrer para depois pedir para o angular calcular o novo tamanho do iframe. Quando menor, melhor para ficar transparente para o usuário
		setTimeout(() => {
			this.resizeIFrame(element)
		})
	}

	/**
	 * ClickeFrame indica que o frame foi clicado. Outras situações são durante o carregamento
	 */
	resizeIFrame($event: any) {
		const clickedIframe: any = document.querySelector(`#${$event.view?.frameElement.id}`)
		const [tab] = this.findiFrameTab(clickedIframe?.title || $event.title)

		if (tab?.height) {
			if (clickedIframe) clickedIframe.style.height = `${tab.height}px`
			if (!clickedIframe) $event.style.height = `${tab.height}px`
		}

		if (!tab?.height) {
			if (!clickedIframe) {
				const height =
					$event.contentWindow.document.body.scrollHeight || $event.contentWindow.document.body.offsetHeight
				$event.style.height = `${height}px`
			}

			if (clickedIframe) {
				const height = $event.view.document.body.scrollHeight || $event.view.document.body.offsetHeight
				clickedIframe.style.height = `${height}px`
			}
		}
	}

	findiFrameTab(iframeLabel): any {
		return this.previewTabs.filter((tab) => tab.label === iframeLabel)
	}

	// Gera um ID aleatório para diminuir o conflito de IDs por conta do form para envio ao codePen
	generateRandomID() {
		return Math.floor(Math.random() * 999999999999999)
	}
}
