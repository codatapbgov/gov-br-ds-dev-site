import { Directive, ElementRef, HostListener } from '@angular/core'

@Directive({
	selector: '[navkeyboard]',
})
export class NavKeyboardDirective {
	constructor(private el: ElementRef) {}

	@HostListener('keydown', ['$event'])
	onkeyDown() {
		const list = this.el.nativeElement.parentNode.querySelector('.br-list')
		this._handleKeydownOnList(event, list)
	}

	@HostListener('click', ['$event'])
	onClick() {
		let list = this.el.nativeElement.parentNode.querySelector('.br-list')
		if (list) {
			this._resetFocus(list)
			this.el.nativeElement.parentNode.querySelector('.br-list').classList.remove('d-none')
		}
	}

	_hideList() {
		if (this.el.nativeElement.parentNode.querySelector('.br-list'))
			this.el.nativeElement.parentNode.querySelector('.br-list').classList.add('d-none')
	}
	_resetFocus(list) {
		const itens = list.querySelectorAll('.br-item:not(.load)')
		for (let iFocused = 0; iFocused < itens.length; iFocused++) {
			if (itens[iFocused].dataset.focus == 'focus') {
				itens[iFocused].dataset.focus = ''
				break
			}
		}
	}

	_showList(event) {
		let lista = this.el.nativeElement.parentNode.querySelector('.br-list')
		if (event.keyCode != 27 && event.keyCode != 9 && lista && lista.classList.contains('d-none'))
			lista.classList.remove('d-none')
	}

	_handleKeydownOnList(event, list) {
		switch (event.keyCode) {
			case 38:
				this._getPreviousItem(list)
				event.preventDefault()
				break
			case 40:
				this._getNextItem(list)
				event.preventDefault()
				break
			case 27:
				this._hideList()
				this._resetFocus(list)
			case 9:
				this._hideList()
				this.el.nativeElement.querySelector('#searchbox').blur()
				this._resetFocus(list)
			default:
				this._showList(event)
		}
	}

	/**
	 * Aplica o estado de focus no próximo elemento
	 * @private
	 */
	_getNextItem(list) {
		if (list) {
			const itens = list.querySelectorAll('.br-item:not(.load)')
			let dataFocus = false
			for (let iFocused = 0; iFocused < itens.length; iFocused++) {
				if (itens[iFocused].dataset.focus == 'focus') {
					dataFocus = true
					if (iFocused < itens.length - 1) {
						itens[iFocused].dataset.focus = ''
						itens[iFocused + 1].dataset.focus = 'focus'
						itens[iFocused + 1].focus()
					}
					break
				}
			}
			if (!dataFocus) {
				dataFocus = true
				itens[0].dataset.focus = 'focus'
				itens[0].focus()
			}
		}
	}
	/**
	 * Aplica o estado de focus no elemento anterior
	 * @private
	 */
	_getPreviousItem(list) {
		if (list) {
			const itens = list.querySelectorAll('.br-item:not(.load)')
			let dataFocus = false
			for (let iFocused = itens.length - 1; iFocused >= 0; iFocused--) {
				if (itens[iFocused].dataset.focus == 'focus' && iFocused > 0) {
					dataFocus = true
					itens[iFocused].dataset.focus = ''
					itens[iFocused - 1].dataset.focus = 'focus'
					itens[iFocused - 1].focus()
					break
				} else if (iFocused == 0) {
					dataFocus = true
					itens[iFocused].dataset.focus = ''
					this.el.nativeElement.querySelector('#searchbox').focus()
				}
			}
			if (!dataFocus) {
				itens[0].dataset.focus = 'focus'
				itens[0].focus()
			}
		}
	}
}
