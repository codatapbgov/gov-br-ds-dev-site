import { Component } from '@angular/core'

@Component({
	selector: 'br-page-not-found',
	templateUrl: './page-not-found.component.html',
})
export class PageNotFoundComponent {}
