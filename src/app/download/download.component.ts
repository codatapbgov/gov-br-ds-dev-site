import { Component, OnInit } from '@angular/core'
import { ISelectedFile } from '../interfaces/selectedFile'
import { ITab } from '../interfaces/tab'
import { StorageService } from '../services/storage.service'
import { environment } from './../../environments/environment.prod'

@Component({
	selector: 'br-downloads',
	styleUrls: ['./download.component.scss'],
	templateUrl: './download.component.html',
})
export class DownloadComponent implements OnInit {
	selectedFile: ISelectedFile
	tabs: ITab[]
	isMarkdown: boolean
	environment = environment
	instance = []

	constructor(private storageService: StorageService) {}

	ngOnInit() {
		this.storageService.currentSelectedFile.subscribe((selectedFile: ISelectedFile) => {
			this.selectedFile = selectedFile
		})
	}
}
