import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Component, ElementRef, Input, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { NavigationEnd, Router } from '@angular/router'
import { CookieService } from 'ngx-cookie-service'
import { throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { FilesService } from '../services/files.service'

@Component({
	selector: 'br-avaliacao',
	templateUrl: './avaliacao.component.html',
	styleUrls: ['./avaliacao.component.scss'],
})
export class AvaliacaoComponent implements OnInit {
	showMenuLinks: boolean
	showSucesso: boolean
	isUP: boolean // Verifica se o servidor está ativo
	votadoUP: boolean // Usuario selecionou gostou
	votadoDOWN: boolean // Usuario selecionou não gostou
	errorEmptyOption: boolean
	URLvoted: string
	feedBackOptions: any
	contactForm: any
	feedback: any
	tipoavaliacao: any
	urlApi: any
	urlUP: any
	cookieValue: any
	voted: boolean

	@Input() public tipo: any

	form = new FormGroup({
		feedbackcomment: new FormControl('teste'),
		opcoes: new FormControl('', [Validators.required]),
	})

	constructor(
		private fileService: FilesService,
		private router: Router,
		private httpClient: HttpClient,
		private elementRef: ElementRef,
		private cookieService: CookieService
	) {
		this.router = router
		this.URLvoted = window.location.href
		// const URLAPI = 'http://localhost:3002'
		const URLAPI = `https://api-avaliacao-ds.estaleiro.serpro.gov.br`

		this.urlUP = URLAPI + `/up/`
		this.urlApi = URLAPI + `/feed/`
		router.events.subscribe((val) => {
			if (val instanceof NavigationEnd) {
				this.reset()
			}
		})

		this.tipoavaliacao = this.elementRef.nativeElement.getAttribute('tipoavaliacao')
		this.contactForm = new FormGroup({
			'feedback-comment': new FormControl(),
		})

		// this.verifyVoted()
	}

	/**
	 * Verifica se votou e desaparece o componente
	 */
	verifyVoted() {
		if (this.tipo !== undefined) {
			const votedURL = this.cookieService.get(this.getRota())
			if (votedURL.length > 0) {
				this.voted = true
			}
			return
		}
		const votedURL = this.cookieService.get(this.router.url)
		if (votedURL === this.router.url) {
			this.voted = true
		} else {
			this.voted = false
		}
	}
	/**
	 * Retorna chave do cookie com tipo
	 * @returns string rota do cookie
	 */

	getRota() {
		return this.router.url.split('?')[0] + this.tipo
	}

	/**
	 * 	Verifica se o servidor da api de avaliação esteja no ar
	 */
	verifyApi() {
		this.isUP = false
		this.avaliacaoData(this.urlUP, {
			session: '',
			like: 1,
		}).subscribe((data: any) => {
			if (!this.voted) {
				this.isUP = true
			}
		})
	}
	/**
	 * Envia avaliação positiva
	 */
	up() {
		this.showMenuLinks = false

		if (this.votadoUP === false) {
			this.votadoUP = true
			this.votadoDOWN = false
			this.avaliacaoData(this.urlApi, {
				session: '',
				like: 1,
			}).subscribe((data: any) => {
				this.showSucesso = true
				this.setVotedCookies()
			})
		}
	}
	/**
	 * Adiciona voto com endereço do cookir
	 */
	setVotedCookies() {
		let expiredDate = new Date()
		const EXPIRED_DAYS = 12
		expiredDate.setDate(expiredDate.getDate() + EXPIRED_DAYS)

		let votedJson = this.router.url
		if (this.tipo !== undefined) {
			const rota = this.getRota()
			votedJson = JSON.stringify(this.tipo)
			this.cookieService.set(rota, votedJson, expiredDate)
		} else {
			this.cookieService.set(this.router.url, votedJson, expiredDate)
		}
	}
	/**
	 * Envia avaliação negativa
	 */
	down() {
		this.showMenuLinks = true
		if (this.votadoDOWN === false) {
			this.votadoUP = false
			this.votadoDOWN = true
		}
	}

	sendComment() {
		if (this.form.valid) {
			this.errorEmptyOption = false
			this.avaliacaoData(this.urlApi, {
				session: '',
				like: 0,
				comentario: this.form.value.feedbackcomment,
				question: this.form.value.opcoes,
			}).subscribe((data: any) => {
				this.showSucesso = true
				this.setVotedCookies()
				this.showMenuLinks = false
			})
		} else {
			this.errorEmptyOption = true
		}
	}
	/**
	 *
	 * @param url endereço da url da ai
	 * @param data
	 * @returns
	 */

	private avaliacaoData(url: string, data: any) {
		const options: any = { responseType: 'json' }
		let mensagem: any = ''

		return this.httpClient
			.post<string[]>(
				url,
				{
					session: data.session,
					like: data.like > 0 ? true : false,
					document: window.location.href,
					comentario: data.comentario,
					question: data.question,
				},
				options
			)
			.pipe(catchError(this.handleError))
	}

	private avaliacaoServer(url: string, data: any) {
		const options: any = { responseType: 'json' }
		let mensagem: any = ''

		return this.httpClient
			.post<string[]>(
				url,
				{},

				options
			)
			.pipe(catchError(this.handleError))
	}

	private handleError(error: HttpErrorResponse) {
		return throwError(`Não foi possível acessar o servidor: ${error.url} `)
	}

	private reset() {
		this.showMenuLinks = false
		this.showSucesso = false
		this.verifyApi()
		this.votadoUP = false
		this.votadoDOWN = false

		this.form.reset()
	}

	private listOptions() {
		const dev = [
			{ id: 1, name: 'Preciso de exemplos mais práticos' },
			{ id: 2, name: 'Não encontrei a informação que eu desejava' },
			{ id: 3, name: 'O código apresenta erro ou não funciona como eu esperava' },
			{ id: 4, name: 'As informações ou exemplos não estão claros.' },
		]
		const des = [
			{ id: 5, name: 'O documento não atende a minha necessidade.' },
			{ id: 6, name: 'Há erro ou inconsistência na documentação.' },
			{ id: 7, name: 'As informações ou exemplos não estão claros.' },
			{ id: 8, name: 'Não encontrei a informação que procuro.' },
		]

		const fundamentosvisuais = [
			{ id: 9, name: 'O documento não atende a minha necessidade.' },
			{ id: 10, name: 'Há erro ou inconsistência na documentação.' },
			{ id: 11, name: 'As informações ou exemplos não estão claros.' },
			{ id: 12, name: 'Não encontrei a informação que procuro.' },
		]

		const utilitarios = [
			{ id: 13, name: ' O documento não atende a minha necessidade.' },
			{ id: 14, name: ' O código apresenta erro ou não funciona como o esperado.' },
			{ id: 15, name: ' As orientações sobre atributos/propriedades/classes não estão claras.' },
			{ id: 16, name: 'Não encontrei a informação sobre atributos/propriedades/classes.' },
		]

		this.tipoavaliacao = this.elementRef.nativeElement.getAttribute('tipoavaliacao')

		switch (this.tipo) {
			case 1:
				this.feedBackOptions = dev
				break
			case 2:
				this.feedBackOptions = des
				break
			case 3:
				this.feedBackOptions = fundamentosvisuais
				break
			default:
				this.feedBackOptions = dev
		}
	}

	ngOnInit(): void {
		this.reset()

		this.listOptions()
		this.verifyApi()
		this.verifyVoted()
	}
}
