import { Location } from '@angular/common'
import { Component, Input, OnChanges } from '@angular/core'
import { Router } from '@angular/router'
import { ITab } from 'src/app/interfaces/tab'
import { NavigationService } from '../services/navigation.service'

@Component({
	selector: 'br-tabs',
	templateUrl: './tabs.component.html',
})
export class TabsComponent implements OnChanges {
	@Input() tabs: ITab[]
	activeTab: ITab

	constructor(private router: Router, private navigationService: NavigationService, private location: Location) {}

	ngOnChanges() {
		// Converte para array caso seja um objeto
		if (!Array.isArray(this.tabs)) this.tabs = [this.tabs]
		this.setActiveTabByUrl()
		this.selectActiveTabOnInit()
	}

	// FIXME O componente não trata quando nenhuma ou mais de uma aba vem selecionada
	setActiveTabOnClick(id: number) {
		this.tabs.map((tab) => {
			if (tab.id === id) {
				tab.isActive = true
				if (tab.showOnUrl) this.setTabUrl(id)
			}
			if (tab.id !== id) tab.isActive = false
		})
	}

	setTabUrl(id: number) {
		let cleanUrlTree = this.router.parseUrl(this.router.url)
		cleanUrlTree.queryParams = {}
		cleanUrlTree.fragment = null

		const tab = this.findTab(id)
		const url = this.createUrl(cleanUrlTree.toString(), tab.label)

		this.location.replaceState(`${url}`)
	}

	findTab(id: number): ITab {
		return this.tabs.find((tab) => tab.id === id)
	}

	createUrl(route: string, queryParam: string) {
		let url = route

		if (queryParam) {
			url += `?tab=${queryParam.toLowerCase()}`
		}

		return url
	}

	selectActiveTabOnInit() {
		// const hasActiveTab = this.tabs?.some((tab) => tab.isActive === true)
		const hasActiveTab = this.tabs?.filter((tab) => tab.isActive)
		this.activeTab = hasActiveTab[0]
		if (hasActiveTab.length === 0 && this.tabs) {
			this.tabs[0].isActive = true
			this.activeTab = this.tabs[0]
		}

		if (this.activeTab.showOnUrl) this.setTabUrl(this.activeTab.id)
	}

	/**
	 * Busca na url o queryParam e seta a aba com o mesmo label como ativa
	 */
	setActiveTabByUrl() {
		let params = this.navigationService.getQueryParam()

		if (params[1]) {
			this.tabs.map((tab) => {
				if (tab.label.toLowerCase() === params[1]) {
					tab.isActive = true
					this.activeTab = tab
				}
				if (tab.label.toLowerCase() !== params[1]) tab.isActive = false
			})
		}
	}
}
