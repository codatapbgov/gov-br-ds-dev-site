import { Injectable } from '@angular/core'

@Injectable({
	providedIn: 'root',
})
export class UtilService {
	removeSpecialChars(str: string) {
		let replacedString: string = str

		replacedString = str
			.replace(/_+/gm, '_')
			.replace(/[áàãâäã]/gm, 'a')
			.replace(/[ç]/gm, 'c')
			.replace(/[éèêë]/gm, 'e')
			.replace(/[íìîï]/gm, 'i')
			.replace(/[óòõôö]/gm, 'o')
			.replace(/[úùûü]/gm, 'u')
			.replace(/\s/g, '')

		return replacedString
	}

	copyText(textToCopy: string) {
		const fakeTextArea = document.createElement('textarea')

		// Evita a renderização na tela
		fakeTextArea.style.height = '0px'
		fakeTextArea.style.opacity = '0'
		fakeTextArea.style.width = '0px'

		document.body.appendChild(fakeTextArea)

		fakeTextArea.value = textToCopy

		this.copyTextFromElement(fakeTextArea)
	}

	copyTextFromElement(element: any) {
		element.select()
		document.execCommand('copy')
		element.setSelectionRange(0, 0)
		element.remove()
	}

	toTitleCase(word: string) {
		if (!word) return word
		return word[0].toUpperCase() + word.substr(1).toLowerCase()
	}
}
