import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'

@Injectable({
	providedIn: 'root',
})
export class FilesService {
	constructor(private httpClient: HttpClient) {}

	/**
	 * @param url URL do recurso
	 * @param responseType Tipo da resposta esperada ['arraybuffer'|'blob'|'json' (default)|'text']
	 * @returns Observable
	 */
	fetchData(url: string, responseType?: string) {
		const options: any = { responseType: 'text' }

		if (!responseType) options.responseType = 'json'

		return this.httpClient.get<string[]>(url, options).pipe(catchError(this.handleError))
	}

	avaliacaoData(url: string, data: any) {
		const options: any = { responseType: 'json' }
		let mensagem: any = ''
		console.log('---consulata-')
		return this.httpClient
			.post<string[]>(url, { session: 'teste', like: 1, document: 'button', comentario: 'teste' }, options)
			.pipe(catchError(this.handleError))
	}

	fixUrls(content: string, fileUrl: string) {
		// Remove a parte final da URL (component.md)
		fileUrl = this.replaceUrls(fileUrl, '(.*)/.*.(md|css|js|html)', '$1')

		const regEx = [
			{
				// Imagens
				find: '((imagens|videos|assets)/.*?.(gif|bmp|tif|tiff|png|jpg|jpeg|svg|ico|webp|mp4|css|scss|js))',
				replace: `${fileUrl}/$1`,
			},
		]

		regEx.forEach((expression) => {
			content = this.replaceUrls(content, expression.find, expression.replace)
		})

		return content
	}

	replaceUrls(data: string, find: string, replace: string): string {
		return data.replace(new RegExp(find, 'gm'), replace)
	}

	private handleError(error: HttpErrorResponse) {
		return throwError(`Não foi possível encontrar o arquivo: ${error.url}`)
	}
}
