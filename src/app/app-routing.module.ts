import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ContentComponent } from './content/content.component'
import { DownloadComponent } from './download/download.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'

const routes: Routes = [
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
	{
		path: 'downloads/:id',
		component: DownloadComponent,
	},
	{
		path: ':section/:id',
		component: ContentComponent,
	},
	{
		path: ':section/:subsection/:id',
		component: ContentComponent,
	},
	{
		path: ':id',
		component: ContentComponent,
	},
	{ path: '**', component: PageNotFoundComponent },
]

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			anchorScrolling: 'enabled', // scrolls to the anchor element when the URL has a fragment
			scrollOffset: [0, 192], // scroll offset when scrolling to an element (optional)
			scrollPositionRestoration: 'enabled', // restores the previous scroll position on backward navigation
			useHash: false,
			enableTracing: false,
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
