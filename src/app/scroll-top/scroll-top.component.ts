import { Component, HostListener } from '@angular/core'

@Component({
	selector: 'br-scroll-top',
	templateUrl: './scroll-top.component.html',
	styleUrls: ['./scroll-top.component.scss'],
})
export class ScrollTopComponent {
	windowScrolled: boolean

	constructor() {}

	@HostListener('window:scroll', [])
	onWindowScroll() {
		if (window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop > 100) {
			this.windowScrolled = true
		} else if (
			(this.windowScrolled && window.pageYOffset) ||
			document.documentElement.scrollTop ||
			document.body.scrollTop < 10
		) {
			this.windowScrolled = false
		}
	}

	scrollToTop() {
		window.scrollTo(0, 0)
	}
}
