import { Directive, ElementRef, HostListener } from '@angular/core'

@Directive({
	selector: '[brCollapse]',
})
export class CollapseDirective {
	constructor(private el: ElementRef) {}

	@HostListener('click') onClick() {
		const target = this.el.nativeElement.parentElement.querySelector(
			`#${this.el.nativeElement.getAttribute('data-target')}`
		)
		const icon = this.el.nativeElement.querySelector('i.fas')
		if (target.hasAttribute('hidden')) {
			target.removeAttribute('hidden')
			icon.classList.remove('fa-chevron-down')
			icon.classList.add('fa-chevron-up')
		} else {
			target.setAttribute('hidden', '')
			icon.classList.remove('fa-chevron-up')
			icon.classList.add('fa-chevron-down')
		}
	}
}
