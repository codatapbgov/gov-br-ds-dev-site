import { SafeHtml, SafeResourceUrl } from '@angular/platform-browser'

export interface IPreviewTab {
	id: number
	label: string
	path: string
	url: SafeResourceUrl
	content?: SafeHtml
	unsafeContent?: string
	isPreview: Boolean
	lang?: string
	height?: number
}
