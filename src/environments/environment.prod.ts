export const environment = {
	production: true,
	debugMode: false,
	baseUrl: './docs/',
	docsUrl: 'docs',
	docUrl: './docs/',
	designDocUrl: './docs/',
	YOUR_GTM_ID: 'GTM-NPF649K',
	HJID: '1673352',
}
