export const environment = {
	production: true,
	debugMode: false,
	baseUrl: 'http://192.168.41.128:9000/',
	docsUrl: 'docs',
	designDocUrl: 'http://192.168.41.128:9000/',
	docUrl: 'http://192.168.41.128:9000/',
	YOUR_GTM_ID: 'GTM-NPF649K',
	HJID: '1673352',
}
