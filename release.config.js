const sharedConfig = require('@govbr-ds/release-config')
// TODO: Parar de usar o standard-version no projeto para usar o semantic-release
module.exports = {
	branches: [...sharedConfig.branches],
	plugins: [
		sharedConfig.plugins.commitAnalyzer,
		sharedConfig.plugins.releaseNotes,
		sharedConfig.plugins.changelog,
		sharedConfig.plugins.gitlab,
		sharedConfig.plugins.git,
	],
}
