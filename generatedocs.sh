#!/usr/bin/env bash


rm -r -f docslocal
mkdir  docslocal
cd docslocal
rm -f -R docs
mkdir docs
mkdir -p docs/govbr-ds-design-diretrizes/componentes
mkdir docs/fundamentos-visuais
mkdir docs/components
cp -r ../../govbr-ds-site-content/* docs

echo "copiado govbr-ds-site-content"
    
cp -r ../../govbr-ds-design-diretrizes/componentes/* docs/components
cp -r ../../govbr-ds-design-diretrizes/componentes/* docs/govbr-ds-design-diretrizes/componentes
cp -r ../../govbr-ds-design-diretrizes/fundamentos/* docs/fundamentos-visuais

echo "copiado govbr-ds-design-diretrizes"

mkdir -p docs/padroes/design
cp -r ../../govbr-ds-design-diretrizes/padroes/* docs/padroes/design
cp -r ../../govbr-ds-dev-diretrizes/* docs

echo "copiado govbr-ds-design-diretrizes/padroes/ e govbr-ds-dev-diretrizes"

mkdir docs/padroes/writing
mkdir docs/padroes/writing/principios-writing
cp -r ../../govbr-ds-writing/principios-writing/* docs/padroes/writing/principios-writing
mkdir -p docs/padroes/writing/microcopy
cp -r ../../govbr-ds-writing/microcopy/* docs/padroes/writing/microcopy
mkdir docs/acessibilidade
cp -r ../../govbr-ds-acessibilidade/acessibilidade/* docs/acessibilidade
cp -r ../../govbr-ds-design-diretrizes/padroes/erro/imagens docs/templates/erro
cp -r ../../govbr-ds-design-diretrizes/padroes/base/imagens docs/templates/base

echo "copiado docs/padroes/writing"

mkdir docs/ds
echo "instal pbgov-ds-core"
npm install --prefix ../../govbr-ds-dev-core
echo "build pbgov-ds-core"
npm run  build --prefix ../../govbr-ds-dev-core
echo "Copiando pbgov-ds-core"
cp -r ../../govbr-ds-dev-core/dist docs/ds
cp ../../govbr-ds-dev-core/package.json docs
echo "Arquivos copiados para docslocal."
# ls
